package com.example.mockito;



//import static org.junit.Assert.assertThat;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.platform.commons.annotation.Testable;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.demo.controller.UserController;
import com.demo.dao.UserRepository;
import com.demo.dao.UserRepositoryImpl;
import com.demo.entity.User;
import com.demo.service.UserService;
import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)                       //code coverage increases due to this
public class TestUser {
	private MockMvc mockMvc;
	User u=null;
	@Mock
	private UserRepositoryImpl userRepository;
	@Mock
	private UserService userservice;
	
	
	@Rule public MockitoRule rule=MockitoJUnit.rule();
	
	@Before
	public void setUp() {
		u=new User();
	}
	
	@InjectMocks
	private UserController usercontroller;
	@InjectMocks
	private UserService repo;
	@InjectMocks
	private UserRepositoryImpl userRepo;
	
	
	@Test
	public void SaveUsertest() throws Exception {
		User user=new User();
		user.setId(10);
		user.setFname("anuu");
		user.setLname("gupta");
		user.setEmail("anu@gmail.com");
			User user1=userRepository.saveUser(user);
		 assertThat(usercontroller.addUser(user)).isNull();
		assertEquals(user1,userRepository.saveUser(user));
		when(userservice.setUser(user)).thenReturn(user);
		 User savedUser = repo.setUser(new User(1,"priya","sharma","priya@gmail.com"));
		 
		
	
	     
		  
	}
	
	@Test
	public void getusertest() {
		User user1=userRepository.getById(anyInt());
		assertEquals(user1,userRepository.getById(anyInt()));
		User user= userservice.getById(anyInt());
		when(userservice.getById(anyInt())).thenReturn(user);
		when(userRepository.getById(anyInt())).thenReturn(user);
		
		 assertThat(usercontroller.getUser(anyInt())).isNull();
		assertEquals(user,userservice.getById(anyInt()));
		
		
	}
	
	@Test
	public void updateUsertest() {
		User user=new User();
		
		user.getEmail();
		user.getFname();
		user.getId();
		user.getLname();
		User user1=userRepository.updateUser(user);
		assertEquals(user1,userRepository.updateUser(user));
			
		when(userservice.updateUser(user)).thenReturn(user);
		when(userRepository.updateUser(user)).thenReturn(user);
		when(usercontroller.updateUser(user)).thenReturn(user);
		 
	}
	
	
	@Test
	public void ListofUsers() {
		List<User> user1=userRepository.allUsers();
		assertEquals(user1,userRepository.allUsers());
		List<User> Alluser=userservice.allUsers();
		when(userservice.allUsers()).thenReturn(Alluser);
		when(usercontroller.getUsers()).thenReturn(Alluser);
		when(userRepository.allUsers()).thenReturn(Alluser);
	
	}
	
	@Test
	public void deleteusertest() {
		String user1=userRepository.deleteById(anyInt());
		assertEquals(user1,userRepository.deleteById(anyInt()));
		userservice.deleteById(anyInt());
		when(userservice.deleteById(anyInt())).thenReturn("true");
		when(usercontroller.deleteUser(anyInt())).thenReturn("true");
		when(userRepository.deleteById(anyInt())).thenReturn("true");
		
		 
	}
	@Test
	public  void testconstructor()
	{
		User u=new User(10,"priya","sharma","priya@gmail.com");
		assertEquals(10,u.getId());
		assertEquals("priya",u.getFname());
		assertEquals("sharma",u.getLname());
		assertEquals("priya@gmail.com",u.getEmail());
		
		
	}
	
	

	@Test
	public void showuserpostTest() throws Exception {
		   User user = new User(100,"annu","gupta","annu@gmail.com");
		   Mockito.when(userservice.setUser(Mockito.any(User.class))).thenReturn(user);
		
		  Gson g = new Gson();
		 
		   
		RequestBuilder requestBuilder = MockMvcRequestBuilders
        .post("/user")
        .accept(MediaType.APPLICATION_JSON)
        .content( g.toJson(user))
        .contentType(MediaType.APPLICATION_JSON);

		
	}
	
	
	/*public void showusergetTest() throws Exception {
		  User user = new User(100,"annu","gupta","annu@gmail.com");
		   Mockito.when(userservice.allUsers()).thenReturn((List<User>) u);
			
		 
		   
		  RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
					"/user/{id}").accept(
					MediaType.APPLICATION_JSON);
		  MvcResult result = mockMvc.perform(requestBuilder).andReturn();

			//System.out.println(result.getResponse());
			String expected = "{id:100,fname:annu,lname:gupta,email:annu@gmail.com}";

		
			JSONAssert.assertEquals(expected, result.getResponse()
					.getContentAsString(), false);

		
	}*/
	
}