package com.demo.service;


	import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;

	import com.demo.entity.User;
	import com.demo.dao.UserRepository;

	@Service
	public class UserService {
	    @Autowired
	    private UserRepository repo;

	    public User setUser(User us) {
	     
	        return repo.saveUser(us);

	    }
	    public User updateUser(User us) {
	       
	        return repo.updateUser(us);

	    }

	    public  User getById(int id)
	    { 
	    return repo.getById(id);}
	    
	   public String deleteById(int id)
	   {
		  
		   return repo.deleteById(id);}
	   
	   public  List<User> allUsers()
	   { 

	   return repo.allUsers();
	   }

}
