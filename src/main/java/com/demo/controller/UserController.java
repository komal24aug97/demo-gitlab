package com.demo.controller;

import com.demo.entity.User;
import com.demo.service.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;



@RestController

public class UserController {
	 
    @Autowired
  
    private UserService userservice;
    
    @PostMapping("/user")
    public User addUser(@RequestBody User user) {

        return userservice.setUser(user);

    }

    @PutMapping("/user")
    public User updateUser(@RequestBody User user) {

        return userservice.updateUser(user);

    }

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable("id") int id) {
        return userservice.getById(id);
    }

    @GetMapping("/users")
    public List<User> getUsers() {
        return userservice.allUsers();
    }

    @DeleteMapping("/user/{id}")
    public String deleteUser(@PathVariable("id") int id){
        return userservice.deleteById(id);
    }

}
