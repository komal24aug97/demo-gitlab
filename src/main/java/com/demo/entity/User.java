package com.demo.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Builder;
import lombok.Data;



@Data

@Entity
@Builder
@Table(name="user")
public class User {
   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="fname" ,nullable=false)
    private String fname;
	@Column(name="lname" ,nullable=false)
    private String lname;
	@Column(name="email" ,nullable=false)
    private String email;
    
	public User() {
	
		this.id=id;
		this.fname=fname;
		this.lname=lname;
		this.email=email;
		
	}
	
	public User(int id, String fname, String lname, String email) {
		
		this.id=id;
		this.fname=fname;
		this.lname=lname;
		this.email=email;
		
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
    
	
}