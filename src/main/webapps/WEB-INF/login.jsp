<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>User login</title>
</head>
<body>
    <div style="text-align: center">
        <h1>User Login</h1>
      
        <form:form accept-charset="UTF-8" modelAttribute="User"
					action="login.html" method="post">
        
        <spring:bind path="email">
            <label for="email">Email:</label>
            <input name="email" size="30" />
            <c:forEach var="emid" items="${email}">
			<form:option value="${emid.email}"
			label="${emid.email} : ${emid.email} " />
			</c:forEach>
         </spring:bind>
            <br><br>
            
            <label for="password">Password:</label>
            <input type="password" name="password" size="30" />
            <br>${message}
            <br><br>           
            <button type="submit">Login</button>
        </form:form>
    </div>
</body>
</html>
